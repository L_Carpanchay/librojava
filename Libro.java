public class Libro {

    private Long isbn;
    private String titulo;
    private String autor;
    private Integer numeroDePaginas;

    public void setISBN(Long visbn) {
        this.isbn=visbn;
    }
    public Long getISBN() {
        return isbn;
    }

    public void setTitulo(String vtitulo) {
        this.titulo=vtitulo;
    }
    public String getTitulo() {
        return titulo;
    }

    public void setAutor(String vautor) {
        this.autor=vautor;
    }
    public String getAutor() {
        return autor;
    }

    public void setNumeroDePaginas(Integer vnumdepagina) {
        this.numeroDePaginas=vnumdepagina;
    }
    public Integer getNumeroDePaginas() {
        return numeroDePaginas;
    }

    public void MostrarxPantalla() {
        System.out.println("El libro "+titulo+" con ISBN "+isbn+", creado por el autor: "+autor+", tiene "+numeroDePaginas+" paginas");
    }
}
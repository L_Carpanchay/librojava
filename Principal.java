public class Principal {
    
    public static void main (String[] args) {
        Libro libro1 = new Libro();
        Libro libro2 = new Libro();
        
        libro1.setISBN(1000L);
        libro1.setTitulo("Detective Conan: Moonlight Sonata Murder Case");
        libro1.setAutor("Gosho Aoyama");
        libro1.setNumeroDePaginas(60);
        libro1.MostrarxPantalla();
        libro2.setISBN(9789871789429L);
        libro2.setTitulo("A Study in Scarlet");
        libro2.setAutor("Arthur Conan Doyle");
        libro2.setNumeroDePaginas(240);
        libro2.MostrarxPantalla();
        
    }
}